var header_UploadOut = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_UploadOut",
        body: 
        {
        	id:"UploadOut_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"UploadOut_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {
                                        view:"button",value:"Upload (อัพโหลด)",width:200,id:"uploadOut_pop_upload",on:
                                        {
                                            onItemClick:function(id, e)
                                            {
                                                $$('uploadOut_win_upload').show();
                                            }
                                        }
                                    },{}                          
                                ]
                        },
                        {
                            view:"form",
                            paddingY:20,
                            id:"UploadOut_form2",
                            elements:
                            [
                                {                            
                                    cols:                                    
                                    [    
                                        {
                                            view:"button", label:"Confirm Upload (ยืนยันการอัพโหลด)",type:"form",width:270,id:"confirm_upload",
                                            on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        var dataT1 = $$("showUploadout_dataT1");
                                                            if(dataT1.count() >0)
                                                            {
                                                                webix.confirm(
                                                                {
                                                                    title:"<b>ข้อความจากระบบ</b>",ok:'ใช่',cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>บันทึกข้อมูล</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",callback:function(result)
                                                                    {
                                                                        if(result)
                                                                        {
                                                                            var btn = $$('confirm_upload'),
                                                                            obj = {doctype:$$('goodsReceipt_doc_grn').getValue().trim().toUpperCase()};
                                                                            btn.disable();
                                                                            $.post( "inbound/goodsReceipt_insert.php", {obj:obj,type:3})
                                                                                .done(function( data ) 
                                                                                {
                                                                                    btn.enable();
                                                                                    var data = eval('('+data+')');
                                                                                    if(data.ch == 1)
                                                                                    { 
                                                                                        webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                        $.post( "print/grn.php", {data1:data.data})
                                                                                        .done(function( data ) 
                                                                                        {
                                                                                            var data = eval('('+data+')');
                                                                                            if(data.ch == 1)
                                                                                            {
                                                                                              webix.message({ type:"default",expire:7000, text:'ปริ้น GRN สำเร็จ'});
                                                                                            }
                                                                                            else webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});
                                                                                        });
                                                                                        $$('clear_all').callEvent("onItemClick", []);
                                                                            
                                                                                    }
                                                                                    else if(data.ch == 2){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                    else if(data.ch == 9){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){}});}
                                                                                    else if(data.ch == 10){webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:data.data,callback:function(){window.open("login.php","_self");}});}                    
                                                                                });
                                                                        }
                                                                    }
                                                                });
                                                                }
                                                                else{webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'ไม่พบข้อมูลในตาราง',callback:function(){}});}                          
                                                    }
                                                }   
                                        },
                                        {
                                            view:"button",icon:"users",type:"danger",value:"Cancel Upload (ยกเลิกการอัพโหลด)",id:"cancel_uploadout",css:"bt_blue",width:300,
                                                on:
                                                {
                                                    onItemClick:function(id, e)
                                                    {                                                                                                         
                                                        webix.confirm({
                                                        title: "กรุณายืนยัน",ok:"ใช่", cancel:"ไม่",text:"คุณต้องการ<font color='#ca4635'><br><b>ที่จะยกเลิกการอัพโหลด</b></font><br><font color='#27ae60'><b>ใช่</b></font> หรือ <font color='#3498db'><b>ไม่</b></font>",
                                                        callback:function(res)
                                                        {
                                                            if(res)
                                                            {
                                                                var btn = $$('cancel_grn'),
                                                                obj =  $$('goodsReceipt_form1').getValues();
                                                                btn.disable();
                                                                $.post("inbound/goodsReceipt_delete.php",{obj:obj,type:1})
                                                                .done(function( data ) 
                                                                {                                                      
                                                                    btn.enable();
                                                                    $$('goodsReceipt_part_no').setValue('');
                                                                    $$('goodsReceipt_lot_no').setValue('');
                                                                    $$('goodsReceipt_qty').setValue('');
                                                                    $$('goodsReceipt_dcd_no').setValue('');
                                                                    $$('goodsReceipt_total_qty').setValue('');
                                                                                                            
                                                                    $$("receive_dataT1").clearAll();
                                                                            $$("receive_dataT1").getPager().render();
                                                                            Receipt_init();   
                                                                            receive_1();                                                                     

                                                                        });
                                                                    }

                                                                }
                                                            })                            
                                                            
                                                    }
                                                }
                                        },
                                        {}                                         
                                                                 
                                    ]
                        },
                       
                        {
                            paddingX:2,
                            rows:
                            [
                                {  
                                    view:"datatable",
                                    id:"showUploadout_dataT1",
                                    navigation:true,
                                    datatype:"jsarray",
                                    css:"my_style",
                                    pager:"showUploadout_pagerA",
                                    autoheight:true,
                                    resizeColumn:true,
                                    leftSplit:0,
                                    footer:true,
                                    columns:
                                    [                                  
                                        { id:"data0",header:"No",css:"rank",width:50},
                                        { id:"data1",header:[{text :"Ticke Number",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:190},              
                                        { id:"data2",header:[{text :"Shipment ID",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:200},
                                        { id:"data3",header:[{text :"Invoice Number",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:180},
                                        { id:"data4",header:[{text :"Invoice Date",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:180},
                                        { id:"data5",header:[{text :"Part Number",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:200,footer:{text:"<b>Total:</b>",height:25,css:{"text-align":"right"}}},
                                        { id:"data6",header:[{text :"Quantity",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:80,footer:{content:"summColumn",colspan:1}},
                                        { id:"data7",header:[{text :"Price",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:150},
                                        { id:"data16",header:[{text :"Value",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:150},
                                        { id:"data17",header:[{text :"English Description",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:200},
                                        { id:"data8",header:[{text :"Thai Deacription",css:{"text-align":"center"}},{content:"textFilter"}],css:"rank",width:200},
                                       

                                    ],
                                }
                                ,
                                {
                                    type:"wide",
                                        cols:
                                        [
                                            {
                                                view:"pager", id:"showUploadout_pagerA",
                                                template:function(data, common){
                                                    var start = data.page * data.size
                                                    ,end = start + data.size;
                                                    if(data.count == 0) start = 0;
                                                    else start += 1;
                                                    if(end >= data.count) end = data.count;
                                                    var html = "<b>showing "+(start)+" - "+end+" total "+data.count+" </b>";
                                                    return common.first()+common.prev()+" "+html+" "+common.next()+common.last();
                                                },
                                                size:10,
                                                group:5 
                                            }
                                        ]
                                                        
                                }

                            ]
                        }
                    ]
                    
                }
                    
                          
                    ]
                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {
                    webix.ui(
                    {
                        view:"window", move:true,modal:true,id:"uploadOut_win_upload",
                        top:100,position:"center",head:
                        {
                            view:"toolbar", margin:-4,
                            cols:
                            [
                                {view:"label", label: "Upload File" },
                                {},
                                { view:"icon", icon:"times-circle",click:"$$('uploadOut_win_upload').hide();$$('uploadOut_upload').files.clearAll();"}
                            ],
                        },
                        body:
                        {
                            view:"form", scroll:false,id:"uploadOut_win_form1",width:300,
                            elements:
                            [
                                {
                                    cols:
                                    [
                                        {},
                                        {
                                            rows:
                                            [
                                                { 
                                                    view: "uploader", value: 'Select File(กดเพื่อเลือกไฟล์)', 
                                                    multiple:false, autosend:false,
                                                    name:"uploader",width:250,id:"uploadOut_upload",
                                                    link:"uploadOut_list",upload:"order/importexl.php",
                                                    on:{
                                                        onBeforeFileAdd:function(item){
                                                            var type = item.type.toLowerCase();
                                                            if (type != "csv"){
                                                                webix.message("Only xlsx are supported");
                                                                return false;
                                                            }
                                                        },
                                                        onFileUpload:function(item){
                                                        },
                                                        onFileUploadError:function(item){
                                                             webix.alert("Error during file upload");
                                                        },
                                                        onUploadComplete:function(data)
                                                        {
                                                            $$("uploadOut_upload").files.clearAll();
                                                            /*$$('uploadOrder_dataT1').clearAll();
                                                            $$('uploadOrder_dataT1').parse(data.data,"jsarray");*/
                                                            $$('uploadOut_win_upload').hide();
                                                        }
                                                    },
                                                },
                                                {
                                                    view:"list",id:"uploadOut_list", type:"uploader",
                                                    autoheight:true, borderless:true,on:
                                                    {
                                                        onAfterRender:function()
                                                        {
                                                            if(this.count() >0) $$('uploadOut_save').show();
                                                            else $$('uploadOut_save').hide();
                                                        }
                                                    }
                                                },
                                                { view:"button",label:"Upload (อัพโหลด)",id:"uploadOut_save",type:'form',hidden:true, click: function() 
                                                    {
                                                        $$("uploadOut_upload").send(function(response)
                                                        {
                                                            if(response)
                                                                webix.message(response.mms);
                                                        });
                                                    }
                                                }
                                            ]
                                        },{}
                                    ]
                                }
                            ]
                        }
                    });

                    window['UploadOut_1'] = function()  
                    {
                       
                        $$('UploadOut_form2').hide();
                        
                    };
                    UploadOut_1()
                }
            }
        }
    };
};