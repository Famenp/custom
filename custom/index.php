<?php
if(!ob_start("ob_gzhandler")) ob_start();
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
include('start.php');
session_start();
if(empty($_SESSION['xxxID']))
{
	header("Location:login.php");
}else
{
    include('php/connection.php');
    $cBy = $_SESSION['xxxID'];
    if ($result = $mysqli->query("SELECT t1.user_id,concat(t1.user_fName,' ',t1.user_lname)user_fName,t1.user_image,t1.user_permission,concat('{',group_concat(concat('\"',t3.menu_menuUse,'\"',':[',t2.role_viwe,',',t2.role_insert,',',t2.role_update,',',t2.role_del,']') separator ','),'}')role
    from tbl_user t1 left join tbl_rolemaster t2 on t1.user_permission=t2.role_name 
    left join tbl_menu t3 on t2.menu_id = t3.menu_id
    where t1.user_id=$cBy and t1.user_status = 1 group by t1.user_id;")) 
    { 
        if($result->num_rows > 0)
        {
            $data = $result->fetch_object();
            $_SESSION['xxxRole'] = json_decode($data->role);
        }
    }
    
}
$pageID = '1';
$role;
if(isset($_GET['url']))
{
    $url = explode('/',filter_var(rtrim($_GET['url'],'/'),FILTER_SANITIZE_URL));
    $menuName = ($url[0] == '') ? 'homePage' : $url[0];
}
else $menuName='homePage';
/*echo $menuName .' '.$_GET['url'];
$mysqli->close();
exit();
*/
?>
<?php
echo preg_replace('/\s{2,}/', '','
<!DOCTYPE html>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="shortcut icon" href="images/favicon.ico"> 
		<link rel="stylesheet" href="codebase/webix.css" type="text/css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="codebase/theme.siberia.css" type="text/css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="codebase/mycss.css" type="text/css" media="screen" charset="utf-8">
		<script src="codebase/webix.js" charset="utf-8"></script>
        <script src="codebase/webix.proto.js" charset="utf-8"></script>
        <script src="js/jquery-2.1.1.min.js" charset="utf-8"></script>
        <script src="codebase/jquery.slimscroll.min.js" charset="utf-8"></script>
        <script src="js/Blob.js"></script>
        <script src="js/FileSaver.js"></script>
        <script src="js/sha1.js"></script>
		<style>
            .toolbar_title{
                padding:10px 10px;line-height:10px;
                background:#3498db;color:#fff;
                 text-align: center;
            }
            .webix_table_checkbox{
    width:22px;
    height:22px;
    margin-top:5px;
  }

		</style>
		<title></title>
	</head>
	<body>
		<script>
       
'
);
?>        
        <?php
            include('header.php');
        ?>
<?php      
echo preg_replace('/\s{2,}/', '','
        function fDisable(name,type)
        {
            if(type) $$(name).disable();
            else $$(name).enable();
        }
		window.addEventListener("popstate", function(e)
		{
    		var character = e.state;
    		var view = $$("renderTitle");
            for(var i=-1,len=view.getChildViews().length;++i<len;)
            {
                if(view.getChildViews()[i].isVisible() == true)
                {
                    view.getChildViews()[i].hide();
                    if(view.getChildViews()[i].getChildViews()[0].callEvent instanceof Function) view.getChildViews()[i].getChildViews()[0].callEvent("onHide", []);
                }
            }

    		if(header_menuObj[character])
    		{	
    			$$("app:menu").blockEvent();
    			$$("app:menu").select(character);
    			$$("app:menu").unblockEvent();
                if($$(header_menuObj[character]))
                {
                    $$(header_menuObj[character]).show();
                    if($$(header_menuObj[character]).getChildViews()[0].callEvent instanceof Function) $$(header_menuObj[character]).getChildViews()[0].callEvent("onShow", []);
                }
                else
                {
                    view.addView(window[header_menuObj[character]]());
                    $$(header_menuObj[character]).getChildViews()[0].callEvent("onAddView", []);
                }
                var item = $$("app:menu").getSelectedItem();
                if(item) 
                {
                    $$("toolbar_title").parse({text:item.value});
                    document.title = item.value;
                }
                else 
                {
                    $$("toolbar_title").parse({text:"Home (หน้าหลัก)"});
                    document.title = "Home (หน้าหลัก)";
                }
    		}
    		else 
    		{
    			$$(header_menuObj["homePage"]) ? $$(header_menuObj["homePage"]).show() : view.addView(window[header_menuObj["homePage"]]());
    			var urlArray = window.location.href.split("/");
                var urlCut = urlArray[0]+"//"+urlArray[2]+"/"+urlArray[3]+"/";
                $$("app:menu").unselectAll();
                history.replaceState("homePage", null,urlCut);
                $$("toolbar_title").parse({text:"Home (หน้าหลัก)"});
                document.title = "Home (หน้าหลัก)";
    		}
    		
    	});	
		</script>
        <script src="main.js" charset="utf-8"></script>
        <script>
        	var menuName ="'.$menuName.'";
        	if(header_menuObj[menuName])
        	{
        		/*$$(header_menuObj[menuName]) ? $$(header_menuObj[menuName]).show() : $$("renderTitle").addView(window[header_menuObj[menuName]]());*/
                if($$(header_menuObj[menuName]))
                {
                    $$(header_menuObj[menuName]).show()
                }
                else
                {
                    $$("renderTitle").addView(window[header_menuObj[menuName]]());
                    if($$(header_menuObj[menuName]))
                    $$(header_menuObj[menuName]).getChildViews()[0].callEvent("onAddView", []);
                }
				$$("app:menu").blockEvent();
    			$$("app:menu").select(menuName);
    			$$("app:menu").unblockEvent();
                
                var item = $$("app:menu").getSelectedItem();
                if(item)
                {
                    $$("toolbar_title").parse({text:item.value});
                    document.title = item.value;
                } 
                else if($$("profilePopup").getItem(menuName))
                {
                    var menu=$$("profilePopup"),t=menu.getItem(menuName);
                    $$("toolbar_title").parse({text: t.value});
                }
                else 
                {
                    $$("toolbar_title").parse({text:"Home (หน้าหลัก)"});
                    document.title = "Home (หน้าหลัก)";
                }
        	}

            var p = localStorage.getItem("treeMenu") || "[]",treeMenu=$$("app:menu");
            p = eval("("+p+")");
            for(var i=-1,len=p.length;++i<len;)
            {
                if(treeMenu.isBranch(p[i])) treeMenu.close(p[i]);
            }

            $(".menu").slimScroll({
                height: window.innerHeight-53+"px",
                size: "12px",
                distance: "2px",
            });
        </script>
	</body>
</html>');
?>
