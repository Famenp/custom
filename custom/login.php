<?php

if(!ob_start("ob_gzhandler")) ob_start();
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
include('start.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="codebase/webix.js" type="text/javascript" charset="utf-8"></script>
 		<link rel="stylesheet" href="codebase/webix.css" type="text/css" charset="utf-8">
 		<script src="js/jquery-2.1.1.min.js" type="text/javascript" charset="utf-8"></script>
 		<script src="js/sha1.js"></script>
		<style>
			#areaA, #areaB{
				margin: 30px;
			}
		</style>
		<title>LOGIN</title>
	</head>
	<body>

		<form>
			<div id="areaA"></div>
			<div id="areaB"></div>
		</form>
	

		<script type="text/javascript" charset="utf-8">
		webix.ui({
				view:"window",
				height:250,
			    width:350,
			    top:100,
			    head:"Login",
			    position:"center",
				body:{
					view:"form",id:"form",
					elements: [
						{ view:"text", label:'User Name', name:"login",id:"user",labelWidth:85 },
						{ view:"text", label:'Password',type:'password', name:"email",id:"pass",labelWidth:85},
						{ view:"button",type:'form', value: "Login", click:btnClick}
					],
					rules:{
						"email":webix.rules.isNotEmpty,
						"login":webix.rules.isNotEmpty
					}
				}
		}).show();

		webix.UIManager.addHotKey("Enter", function(e) { 
			var obj = webix.UIManager.getNext(e);
			webix.UIManager.setFocus(obj);
		    return false; 
		}, $$('user'));
		webix.UIManager.addHotKey("Enter", function(e) { 
			btnClick();
		    return false; 
		}, $$('pass'));

		function btnClick () 
		{
			if ($$("form").validate())
			{
				var user=$$("user").getValue(),pass=sha1($$("pass").getValue());
				$.post( "chPass.php", { user:user, pass:pass})
  					.done(function( data ) {
  					var re = eval('(' + data + ')');
         		 	if(re.ch == 1)
         		 	{
         		 	  window.open("index.php","_self");
         		 	}
         		 	else
         		 	{
         		 		webix.message({ type:"error", text:"ไม่พบข้อมูลผู้ใช้ โปรดลองอีกครั้ง"});
         		 	}
  				});
				
			}
			else
			{
				webix.alert({title:"<b>ข้อความจากระบบ</b>",ok:'ตกลง',text:'กรุณากรอก Username และ Password',callback:function()
                                                                        {
                                                                            $$('user').focus();
                                                                        }});
			}
			
		}
		</script>
	</body>
</html>