<?php
include 'fpdf.php';
include 'exfpdf.php';
include 'easyTable.php';

$pdf=new exFPDF('P');
$pdf->AddPage(); 
$pdf->AddFont('THSarabun','','THSarabun.php');
$pdf->AddFont('THSarabun','I','THSarabun Italic.php');
$pdf->AddFont('THSarabun','B','THSarabun Bold.php');
$pdf->AddFont('THSarabun','BI','THSarabun Bold Italic.php');

$table=new easyTable($pdf,1,'border:0;font-size:10;font-family:THSarabun;');
$table->rowStyle('align:{R};font-size:10;');
$table->easyCell(iconv('UTF-8','TIS-620',"page 1/1"));
$table->printRow();
$table->endTable();

$table=new easyTable($pdf, 1, 'border:0;font-size:14;');
$table->rowStyle('align:{C};');
$table->easyCell(utf8Th(headder()),'font-family:THSarabun;');
$table->printRow();
$table->endTable(10);
 
$table=new easyTable($pdf, '{120,100,100,50,100,100}', 'width:400;font-size:12; border:0; paddingY:2;font-family:THSarabun;font-style:B;');
$table->rowStyle('align:{LLLLLL};');
$table->easyCell(utf8Th("เลขที่เบิกสินค้าออก"));
$table->easyCell(utf8Th("WTH1700000001"));
$table->easyCell(utf8Th("ลำดับที่"));
$table->easyCell(utf8Th("1"));
$table->easyCell(utf8Th("วันที่ส่งออก"));
$table->easyCell(utf8Th("01-12-2017"));
$table->printRow();

$table->rowStyle('align:{LLLL};');
$table->easyCell(utf8Th("รหัสสินค้าสำเร็จรูป"));
$table->easyCell(utf8Th("FG0001"));
$table->easyCell(utf8Th("ชื่อสินค้าสำเร็จรูป"));
$table->easyCell(utf8Th("LDPE"));
$table->easyCell(utf8Th(""));
$table->easyCell(utf8Th(""));
$table->printRow();

$table->rowStyle('align:{LLLL};');
$table->easyCell(utf8Th("จำนวน"));
$table->easyCell(utf8Th("200 TNE"));
$table->easyCell(utf8Th("น้ำหนัก"));
$table->easyCell(utf8Th("0"));
$table->printRow();

$table->rowStyle('align:{LL};');
$table->easyCell(utf8Th("สูตรการผลิตเลขที่"));
$table->easyCell(utf8Th("PAK1700000001"));
$table->printRow();

$table->rowStyle('align:{L};');
$table->easyCell(utf8Th("น้ำหนักส่วนประกอบต่อหน่วย"));
$table->printRow();
$table->endTable(5);



$table=new easyTable($pdf, '{20,100,80,50,50}', 'width:300;font-size:10; border:1; paddingY:2;font-family:THSarabun;font-style:B;');
$table->rowStyle('align:{CCCCC};');
$table->easyCell(utf8Th("ลำดับที่"));
$table->easyCell(utf8Th("ชื่อวัตถุดิบ"));
$table->easyCell(utf8Th("รหัสวัตถุดิบ"));
$table->easyCell(utf8Th("หน่วย"));
$table->easyCell(utf8Th("จำนวนที่เบิก"));
$table->printRow();

$table->rowStyle('align:{CCCCC};');
$table->easyCell(utf8Th("1"));
$table->easyCell(utf8Th("RM-LDPE"));
$table->easyCell(utf8Th("RM-A001"));
$table->easyCell(utf8Th("TNE"));
$table->easyCell(utf8Th("200"));

$table->printRow();

$table->rowStyle('align:{RCCCC};');
$table->easyCell(utf8Th("รวมทั้งสิ้น"),'colspan:4');
$table->easyCell(utf8Th("200.00"));
$table->printRow();


$table->endTable(20);

$table=new easyTable($pdf,'{150,150}', 'width:300;border:0;font-size:12;');
$table->rowStyle('align:{CC};');
$table->easyCell(utf8Th("ขอรับรองลงบัญชีถูกต้อง\n
						 ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						 ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						 ตำแหน่ง /ประทับตรา"));

$table->easyCell(utf8Th("ตรวจแล้วถูกต้อง\n
						ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						เจ้าหน้าที่ควบคุมคลัง"));
$table->printRow();


 $table->endTable(4);
 
//-----------------------------------------

 $pdf->Output(); 

function headder()
{
	$head = '
			บริษัท..................................
			รายงานการเบิก (บัญชีวัตถุดิบ)
			งวดระหว่าง  01/12/2017  ถึง  31/12/2017
			';
	return $head;
}

function detail()
{

}

 function utf8Th($v)
 {
   return iconv( 'UTF-8','TIS-620//TRANSLIT',$v);
 }
?>