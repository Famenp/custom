<?php
include 'fpdf.php';
include 'exfpdf.php';
include 'easyTable.php';

$pdf=new exFPDF('L');
$pdf->AddPage(); 
$pdf->AddFont('THSarabun','','THSarabun.php');
$pdf->AddFont('THSarabun','I','THSarabun Italic.php');
$pdf->AddFont('THSarabun','B','THSarabun Bold.php');
$pdf->AddFont('THSarabun','BI','THSarabun Bold Italic.php');

$datahead = array(
				array('ลำดับที่','เลขที่ใบนำสินค้าออก'	,'ลำดับในใบขนฯ','วันที่ส่งออก','วันที่ตรวจปล่อย','รายละเอียดสินค้า','รหัสสินค้า','หน่วย','จำนวน','มูลค่า(บาท)')
		);

$data0=array(
		array('1','','','','','','','','','','','')
	);

$table=new easyTable($pdf,1,'border:0;font-size:10;font-family:THSarabun;');
$table->rowStyle('align:{R};font-size:10;');
$table->easyCell(iconv('UTF-8','TIS-620',"page 1/1"));
$table->printRow();
$table->endTable();

$table=new easyTable($pdf, 1, 'border:0;font-size:14;');
$table->rowStyle('align:{C};');
$table->easyCell(utf8Th(headder()),'font-family:THSarabun;');
$table->printRow();
$table->endTable(5);

$table=new easyTable($pdf, '{15,30,30,30,25,30,20,20,20,20}', 'width:400;font-size:12; border:1; paddingY:2;font-family:THSarabun;font-style:B;');
for ($i=0; $i <sizeof($datahead) ; $i++) {
	$table->rowStyle('align:{CCCCCCCCCC};'); 
		for ($j=0; $j < 10 ; $j++) { 
			$table->easyCell(utf8Th($datahead[$i][$j]));;
		}
		$table->printRow();
}
for ($i=0; $i <sizeof($data0) ; $i++) {
	$table->rowStyle('align:{CCCCCCCCCC};'); 
		for ($j=0; $j < 12 ; $j++) { 
			$table->easyCell(utf8Th($data0[$i][$j]));;
		}
		$table->printRow();
}
$table->rowStyle('align:{RCCCCCCCCC};');
$table->easyCell(utf8Th("Total"),'colspan:8');
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->printRow();

$table->rowStyle('align:{RCCCCCCCCC};');
$table->easyCell(utf8Th("Grand total"),'colspan:8');
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->printRow();
$table->endTable(20);

$table=new easyTable($pdf,'{150,150}', 'width:300;border:0;font-size:12;');
$table->rowStyle('align:{CC};');
$table->easyCell(utf8Th("ขอรับรองลงบัญชีถูกต้อง\n
						 ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						 ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						 ตำแหน่ง /ประทับตรา"));

$table->easyCell(utf8Th("ตรวจแล้วถูกต้อง\n
						ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						เจ้าหน้าที่ควบคุมคลัง"));
$table->printRow();


 $table->endTable(4);
 
//-----------------------------------------

 $pdf->Output(); 

function headder()
{
	$head = '
			บริษัท............................................
			รายงานการนำสินค้าออกจากเขตปลอดอากร (บัญชีวัตถุดิบ)
			ประเภทเอกสาร : การทำลาย/การบริจาค
			งวดระหว่าง  01/12/2017  ถึง  31/12/2017
			';
	return $head;
}

function detail()
{

}

 function utf8Th($v)
 {
   return iconv( 'UTF-8','TIS-620//TRANSLIT',$v);
 }
?>