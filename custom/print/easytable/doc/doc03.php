<?php
include '../fpdf.php';
include '../exfpdf.php';
include '../easyTable.php';

$pdf=new exFPDF('L');
$pdf->AddPage(); 
$pdf->AddFont('THSarabun','','THSarabun.php');
$pdf->AddFont('THSarabun','I','THSarabun Italic.php');
$pdf->AddFont('THSarabun','B','THSarabun Bold.php');
$pdf->AddFont('THSarabun','BI','THSarabun Bold Italic.php');

$data0=array(
		array('1','09-12-2017','A000000000001','RM-A002','RM-HDPE','12000','2000','','0','-','-','-','0','2000'),
		array('','09-12-2017','A000000000001','RM-A001','RM-LDPE','5000','1000','','0','22-12-2017','PAK1700000001','WTH1700000002','200','800'),
		array('2','15-12-2017','A000000000002','RM-A003','RM-PP','2000','1000','','200','-','-','-','0','1000'),
		array('','15-12-2017','A000000000002','RM-A004','RM-PS','3000','1000','','300','-','-','-','0','1000')
	);

$table=new easyTable($pdf,1,'border:0;font-size:10;font-family:THSarabun;');
$table->rowStyle('align:{R};font-size:10;');
$table->easyCell(iconv('UTF-8','TIS-620',"page 1/1"));
$table->printRow();
$table->endTable();

$table=new easyTable($pdf, 1, 'border:0;font-size:14;');
$table->rowStyle('align:{C};');
$table->easyCell(utf8Th(headder()),'font-family:THSarabun;');
$table->printRow();
$table->endTable(5);

$table=new easyTable($pdf, '{60,300}', 'font-size:12; border:0; paddingY:2;font-family:THSarabun;font-style:B;');
$table->rowStyle('align:{LL};');
$table->easyCell(utf8Th("รหัสผู้ประกอบกิจการ"));
$table->easyCell(utf8Th("F-52-014-0537-0011"));
$table->printRow();
$table->rowStyle('align:{LL};');
$table->easyCell(utf8Th("ชื่อผู้ประกอบกิจการในเขตปลอดอากร"));
$table->easyCell(utf8Th("บรัษัท ฮัวเหว่ย อินดัสเทรียล จำกัด"));
$table->printRow();
$table->endTable(10);

$table=new easyTable($pdf, '{15,25,25,20,50,20,20,15,15,20,30,30,20,20}', 'width:400;font-size:10; border:1; paddingY:2;font-family:THSarabun;font-style:B;');
$table->rowStyle('align:{CCCCCCCCCCCCCC};');
$table->easyCell(utf8Th("ลำดับที่"));
$table->easyCell(utf8Th("วันที่นำสินค้า เข้าเก็นในคลัง"));
$table->easyCell(utf8Th("เลขที่ใบขนเข้าคลังฯ เลขที่พิธีการขาเข้า"));
$table->easyCell(utf8Th("รหัสวัตถุดิบ"));
$table->easyCell(utf8Th("รายละเอียดของวัตถุดิบ"));
$table->easyCell(utf8Th("ราคาสินค้าเข้า"));
$table->easyCell(utf8Th("ปริมาณนำเข้า"));
$table->easyCell(utf8Th("หน่วย"));
$table->easyCell(utf8Th("น้ำหนัก (KGM)"));
$table->easyCell(utf8Th("วันที่ผลิต"));
$table->easyCell(utf8Th("เลขที่เอกสารการผลิต"));
$table->easyCell(utf8Th("เลขที่ใบเบิก"));
$table->easyCell(utf8Th("ปริมาณที่ เบิก/ออก"));
$table->easyCell(utf8Th("วัตถุดิบคงเหลือ"));
$table->printRow();

for ($i=0; $i <sizeof($data0) ; $i++) {
	$table->rowStyle('align:{CCCCCCCCCCCCCC};'); 
		for ($j=0; $j < 14 ; $j++) { 
			$table->easyCell(utf8Th($data0[$i][$j]));;
		}
		$table->printRow();
}
$table->endTable(20);

$table=new easyTable($pdf,'{150,150}', 'width:300;border:0;font-size:12;');
$table->rowStyle('align:{CC};');
$table->easyCell(utf8Th("ขอรับรองลงบัญชีถูกต้อง\n
						 ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						 ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						 ตำแหน่ง /ประทับตรา"));

$table->easyCell(utf8Th("ตรวจแล้วถูกต้อง\n
						ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						เจ้าหน้าที่ควบคุมคลัง"));
$table->printRow();


 $table->endTable(4);
 
//-----------------------------------------

 $pdf->Output(); 

function headder()
{
	$head = '
			รายงานการความเคลื่อนไหวเขตปลอดอากร (บัญชีวัตถุดิบ)
			งวดระหว่าง  01/12/2017  ถึง  31/12/2017
			เลขประจำตัวผู้เสียภาษี 0735556002243
			';
	return $head;
}

function detail()
{

}

 function utf8Th($v)
 {
   return iconv( 'UTF-8','TIS-620//TRANSLIT',$v);
 }
?>