<?php
include '../fpdf.php';
include '../exfpdf.php';
include '../easyTable.php';

$pdf=new exFPDF('L');
$pdf->AddPage(); 
$pdf->AddFont('THSarabun','','THSarabun.php');
$pdf->AddFont('THSarabun','I','THSarabun Italic.php');
$pdf->AddFont('THSarabun','B','THSarabun Bold.php');
$pdf->AddFont('THSarabun','BI','THSarabun Bold Italic.php');


$data0=array(
		array('1','RM-LDPE','RM-A001','TNE','800.00','A0000000000001','RM','20/12/2016','09/12/2016','4,000.00','0.00','RM01'),
		array('2','RM-HDPE','RM-A002','TNE','800.00','A0000000000001','RM','20/12/2016','09/12/2016','4,000.00','0.00','RM01'),
		array('3','RM-PP','RM-A003','TNE','800.00','A0000000000001','RM','20/12/2016','09/12/2016','4,000.00','0.00','RM01'),
		array('4','RM-PS','RM-A004','TNE','800.00','A0000000000001','RM','20/12/2016','09/12/2016','4,000.00','0.00','RM01')
	);

$table=new easyTable($pdf,1,'border:0;font-size:10;font-family:THSarabun;');
$table->rowStyle('align:{R};font-size:10;');
$table->easyCell(iconv('UTF-8','TIS-620',"page 1/1"));
$table->printRow();
$table->endTable();

$table=new easyTable($pdf, 1, 'border:0;font-size:14;');
$table->rowStyle('align:{C};');
$table->easyCell(utf8Th(headder()),'font-family:THSarabun;');
$table->printRow();
$table->endTable(5);

$table=new easyTable($pdf, '{15,25,25,15,25,30,15,20,20,20,30,20}', 'width:400;font-size:12; border:1; paddingY:2;font-family:THSarabun;font-style:B;');
$table->rowStyle('align:{CCCCCCCCCCCCCC};');
$table->easyCell(utf8Th("ลำดับที่"));
$table->easyCell(utf8Th("ชื่อวัตถุดิบ"));
$table->easyCell(utf8Th("รหัสวัตถุดิบ"));
$table->easyCell(utf8Th("หน่วย"));
$table->easyCell(utf8Th("จำนวนคงเหลือ"));
$table->easyCell(utf8Th("เลขที่ใบขนขาเข้า"));
$table->easyCell(utf8Th("ประเภท"));
$table->easyCell(utf8Th("วันนำเข้า"));
$table->easyCell(utf8Th("วันที่อนุมัติ"));
$table->easyCell(utf8Th("มูลค่า(บาท)"));
$table->easyCell(utf8Th("ภาษีอากรรวม(บาท)"));
$table->easyCell(utf8Th("ตำแหน่ง"));
$table->printRow();

for ($i=0; $i <sizeof($data0) ; $i++) {
	$table->rowStyle('align:{CCCCCCCCCCCCCC};'); 
		for ($j=0; $j < 12 ; $j++) { 
			$table->easyCell(utf8Th($data0[$i][$j]));;
		}
		$table->printRow();
}
$table->endTable(20);

$table=new easyTable($pdf,'{150,150}', 'width:300;border:0;font-size:12;');
$table->rowStyle('align:{CC};');
$table->easyCell(utf8Th("ขอรับรองลงบัญชีถูกต้อง\n
						 ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						 ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						 ตำแหน่ง /ประทับตรา"));

$table->easyCell(utf8Th("ตรวจแล้วถูกต้อง\n
						ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						เจ้าหน้าที่ควบคุมคลัง"));
$table->printRow();


 $table->endTable(4);
 
//-----------------------------------------

 $pdf->Output(); 

function headder()
{
	$head = '
			บริษัท............................................
			รายงานสินค้าคงเหลือเขตปลอดอากร (บัญชีวัตถุดิบ)
			งวดระหว่าง  01/12/2017  ถึง  31/12/2017
			';
	return $head;
}

function detail()
{

}

 function utf8Th($v)
 {
   return iconv( 'UTF-8','TIS-620//TRANSLIT',$v);
 }
?>