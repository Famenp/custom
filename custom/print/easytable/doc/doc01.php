<?php
include '../fpdf.php';
include '../exfpdf.php';
include '../easyTable.php';

$pdf=new exFPDF('L');
$pdf->AddPage(); 
$pdf->AddFont('THSarabun','','THSarabun.php');
$pdf->AddFont('THSarabun','I','THSarabun Italic.php');
$pdf->AddFont('THSarabun','B','THSarabun Bold.php');
$pdf->AddFont('THSarabun','BI','THSarabun Bold Italic.php');

$table=new easyTable($pdf,1,'border:0;font-size:10;font-family:THSarabun;');
$table->rowStyle('align:{R};font-size:10;');
$table->easyCell(iconv('UTF-8','TIS-620',"page 1/1"));
$table->printRow();
$table->endTable();

$table=new easyTable($pdf, 1, 'border:0;font-size:14;');
$table->rowStyle('align:{C};');
$table->easyCell(utf8Th(headder()),'font-family:THSarabun;');
$table->printRow();
$table->endTable(5);
 
$table=new easyTable($pdf, '{15,30,25,20,20,20,50,20,15,15,20,20,20,20}', 'width:400;font-size:10; border:1; paddingY:2;font-family:THSarabun;font-style:B;');
$table->rowStyle('align:{CCCCCCCCCCCCCC};');
$table->easyCell(utf8Th("ลำดับที่"));
$table->easyCell(utf8Th("เลขทีใบขนขาเข้า"));
$table->easyCell(utf8Th("ลำดับในใบขนฯ"));
$table->easyCell(utf8Th("วันนำเข้า"));
$table->easyCell(utf8Th("วันนำเข้าคลัง"));
$table->easyCell(utf8Th("วันที่เจ้าหน้าที่\nApprove"));
$table->easyCell(utf8Th("รายละเอียดสินค้า"));
$table->easyCell(utf8Th("รหัสสินค้า"));
$table->easyCell(utf8Th("หน่วย"));
$table->easyCell(utf8Th("น้ำหนัก"));
$table->easyCell(utf8Th("จำนวน"));
$table->easyCell(utf8Th("มูลค่า"));
$table->easyCell(utf8Th("ภาษีอากรรวม\n(บาท)"));
$table->easyCell(utf8Th("ตำแหน่ง"));
$table->printRow();


for ($i=1; $i <=5 ; $i++) {
	$table->rowStyle('align:{CCCCCCCCCCCCCC};'); 
	$table->easyCell(utf8Th($i));
		for ($j=0; $j < 13 ; $j++) { 
			$table->easyCell(utf8Th(""));
		}
		$table->printRow();
}


$table->rowStyle('align:{RCCCCCCCCCCCCC};');
$table->easyCell(utf8Th("รวม"),'colspan:10');
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th(""));
$table->printRow();

$table->rowStyle('align:{RCCCCCCCCCCCCC};');
$table->easyCell(utf8Th("รวม"),'colspan:10');
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th(""));
$table->printRow();

$table->rowStyle('align:{RCCCCCCCCCCCCC};');
$table->easyCell(utf8Th("รวมทั้งสิ้น"),'colspan:10');
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th("0.00"));
$table->easyCell(utf8Th(""));
$table->printRow();

$table->endTable(20);

$table=new easyTable($pdf,'{150,150}', 'width:300;border:0;font-size:12;');
$table->rowStyle('align:{CC};');
$table->easyCell(utf8Th("ขอรับรองลงบัญชีถูกต้อง\n
						 ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						 ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						 ตำแหน่ง /ประทับตรา"));

$table->easyCell(utf8Th("ตรวจแล้วถูกต้อง\n
						ลงชื่อ . . . . . . . . . . . . . . . . . . . . . . . \n
						ว/ด/ป. . . . . . . . . . . . . . . . . . . . . . . \n
						เจ้าหน้าที่ควบคุมคลัง"));
$table->printRow();


 $table->endTable(4);
 
//-----------------------------------------

 $pdf->Output(); 

function headder()
{
	$head = '
			บริษัท..................................
			รายงานการนำของเข้าเขตปลอดอากร (บัญชีวัตถุดิบ)
			ประเภทเอกสาร : จากในประเทศ (ตามคำร้อง)
			งวดระหว่าง  01/12/2017  ถึง  31/12/2017
			';
	return $head;
}

function detail()
{

}

 function utf8Th($v)
 {
   return iconv( 'UTF-8','TIS-620//TRANSLIT',$v);
 }
?>