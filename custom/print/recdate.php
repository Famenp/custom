<?php
include('../php/connection.php');
require_once('tcpdf/tcpdf.php');
$data = $_REQUEST['data1'];

$q1  = "SELECT tran.DCD_No, tran.Rec_Date,pm.part_supplier,concat(usr.user_fname,' ',usr.user_lname) as name from tbl_transaction tran 
LEFT JOIN tbl_user usr ON tran.user_id = usr.user_id
LEFT JOIN tbl_partmaster pm ON tran.Part_ID = pm.part_id
where tran.Doc_No = '$data' group by tran.Part_ID";

if ($result = $mysqli->query($q1)) 
{
	if ($result->num_rows == 0)
		{
			echo 'ไม่พบ  ในระบบ';
			$mysqli->close();
			exit();
		}
	while ($srow = $result->fetch_assoc()) 
	{
			$crow = $result->num_rows;
	    	$ardetail[] = $srow["DCD_No"].",".$srow["part_supplier"].",".$srow["Rec_Date"].",".$srow["name"];
	}
}

$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Receive Date');//title

$pdf->SetMargins(10, 5, 10,5);
$pdf->SetAutoPageBreak(TRUE, 0);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}
$pdf->setFontSubsetting(true);
$pdf->SetFont('freeserif', '');

for ($i=0; $i < $crow ; $i++) {
 	$pdf->AddPage();
	$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
	$html = createbox($ardetail[$i]);
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}

// $pdf->Output($Doc_no.'.pdf', 'I');
$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0,15);
$pdf->Output("D:\\printfile\\".$randomString.'-Printer_inbound.pdf', 'F');
echo '{"ch":1,"data":"DONE"}';


function createbox($arraydata)
{
	$data= explode(",",$arraydata);
	$foot = '
			<table  border="0" align="center" >
					<tbody>
					<tr>
						<td>
							<p>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span style="font-size: 120px;">Receipt Date</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span style="font-size: 60px;">'.$data[0].'</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span style="font-size: 50px;">Part NO. :'. $data[1].'</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span style="font-size: 40px;">'.$data[2].'</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span>&nbsp;</span><br>
								<span style="font-size: 40px;">'.$data[3].'</span><br>
							</p>
						</td>
					</tr>
					</tbody>
			</table>';

	return $foot;
}

