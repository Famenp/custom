<?php
if(!ob_start("ob_gzhandler")) ob_start();
header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

$date1 = '2014-01-01';
$date2 = '2019-01-01';
// $date2 = '2027-01-01';
$dateRang = join(",",getDatesFromRange($date1,$date2));
echo "insert ignore into dl(`date`) values";
echo $dateRang;
echo ";";
// var_dump(getDatesFromRange($date1,$date2));
function getDatesFromRange($startDate, $endDate)
{
    $return = array("('".$startDate."')");
    $start = $startDate;
    $i=1;
    if (strtotime($startDate) < strtotime($endDate))
    {
       while (strtotime($start) < strtotime($endDate))
        {
            $start = date('Y-m-d', strtotime($startDate.'+'.$i.' days'));
            $return[] = "('".$start."')";
            $i++;
        }
    }

    return $return;
}
?>