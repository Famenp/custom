var header_report1 = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_report1",
        body: 
        {
        	id:"report1_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"report1_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"datepicker", label:"Start date",value:new Date(), name:"start" ,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    {
                                        view:"datepicker", label:"End date",value:new Date(), name:"end",labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },

                                    {
                                        rows:
                                        [
                                            {},
                                            {                                             
                                                view:"button", label:"View (ดูตัวอย่างเอกสาร)",width:230,id:"view_report1",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if($$('report1_form1').validate())
                                                        {
                                                            window.open("print/easytable/doc/doc01.php",'_blank');   
                                                        }                               
                                                    }
                                                }       
                                            }
                                        ]
                                    },{}                               
                                ]
                        },   
                    ]                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};