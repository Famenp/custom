var header_report13 = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_report13",
        body: 
        {
        	id:"report13_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"report13_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"datepicker", label:"Start date",value:new Date(), name:"start" ,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    {
                                        view:"datepicker", label:"End date",value:new Date(), name:"end",labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },

                                    {
                                        rows:
                                        [
                                            {},
                                            {                                             
                                                view:"button", label:"View (ดูตัวอย่างเอกสาร)",width:230,id:"view_report13",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if($$('report13_form1').validate())
                                                        {
                                                           /* var grnno = $$('grn_redoc_grn').getValue();
                                                            window.open("print/grnview.php?grnno="+grnno, '_blank'); */  
                                                        }                               
                                                    }
                                                }       
                                            }
                                        ]
                                    },{}                               
                                ]
                        },   
                    ]                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};