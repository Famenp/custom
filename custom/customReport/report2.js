var header_report2 = function()
{
	return {
        view: "scrollview",
        scroll: "native-y",
        id:"header_report2",
        body: 
        {
        	id:"report2_id",
        	type:"clean",
    		rows:
    		[
    		    {
                    view:"form",
                    paddingY:20,
                    id:"report2_form1",
                    elements:
                    [
                        {                            
                            cols:                                    
                                [    
                                    {  
                                        view:"datepicker", label:"Start date",value:new Date(), name:"start" ,labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },
                                    {
                                        view:"datepicker", label:"End date",value:new Date(), name:"end",labelPosition:"top", stringResult:true,format:webix.Date.dateToStr("%Y-%m-%d"),width:250
                                    },

                                    {
                                        rows:
                                        [
                                            {},
                                            {                                             
                                                view:"button", label:"View (ดูตัวอย่างเอกสาร)",width:230,id:"view_report2",
                                                on:
                                                {                                                   
                                                    onItemClick:function(id, e)
                                                    {
                                                        if($$('report2_form1').validate())
                                                        {
                                                           window.open("print/easytable/doc/doc02.php",'_blank'); 
                                                        }                               
                                                    }
                                                }       
                                            }
                                        ]
                                    },{}                               
                                ]
                        },   
                    ]                    
                }
            ],on:
            {
                onHide:function()
                {
                    
                },
                onShow:function()
                {

                },
                onAddView:function()
                {

                }
            }
        }
    };
};