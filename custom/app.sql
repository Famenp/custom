
CREATE TABLE `tbl_user` (
  `user_id` smallint(5) unsigned NOT NULL auto_increment,
  `user_name` varchar(30) collate utf8_unicode_ci NOT NULL,
  `user_pass` char(40) collate utf8_unicode_ci NOT NULL,
  `user_fName` varchar(35) collate utf8_unicode_ci NOT NULL,
  `user_lname` varchar(35) collate utf8_unicode_ci default NULL,
  `user_image` varchar(100) collate utf8_unicode_ci default 'icon_index.jpg',
  `user_status` tinyint(1) unsigned NOT NULL,
  `user_permission` varchar(20) collate utf8_unicode_ci NOT NULL default '',
  `user_creationDate` datetime NOT NULL,
  `user_createBy` smallint(5) unsigned NOT NULL,
  `user_updateDate` datetime default NULL,
  `user_updateBy` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`user_id`),
  KEY `user_pass` (`user_pass`,`user_name`,`user_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into `tbl_user`(`user_id`,`user_name`,`user_pass`,`user_fName`,`user_lname`,`user_image`,`user_status`,`user_permission`,`user_creationDate`,`user_createBy`,`user_updateDate`,`user_updateBy`) values (1,'numstar','9e781a4ba73d1860ec85f5ded49932d69e18822f','Apichat','Peekoa','noom.jpg',1,'ADMIN','2015-06-15 09:41:51',1,null,null);
insert into `tbl_user`(`user_id`,`user_name`,`user_pass`,`user_fName`,`user_lname`,`user_image`,`user_status`,`user_permission`,`user_creationDate`,`user_createBy`,`user_updateDate`,`user_updateBy`) values (2,'pw8605','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','Penthip','.P','PW8605.jpg',1,'SUPPORT','2015-11-22 20:51:28',1,null,null);
insert into `tbl_user`(`user_id`,`user_name`,`user_pass`,`user_fName`,`user_lname`,`user_image`,`user_status`,`user_permission`,`user_creationDate`,`user_createBy`,`user_updateDate`,`user_updateBy`) values (3, 'yumi', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 'Jitiporn', ' Aussiwakul', 'JA3579.jpg', 1, 'SUPPORT', '2015-06-15 09:41:51', 1, NULL, NULL);

CREATE TABLE `tbl_rolemaster` (
  `role_id` smallint(5) unsigned NOT NULL auto_increment,
  `role_name` varchar(30) collate utf8_unicode_ci default '',
  `role_viwe` tinyint(1) unsigned NOT NULL default '0',
  `role_insert` tinyint(1) unsigned NOT NULL default '0',
  `role_update` tinyint(1) unsigned NOT NULL default '0',
  `role_del` tinyint(1) unsigned NOT NULL default '0',
  `role_creationDate` datetime NOT NULL,
  `role_createBy` smallint(5) unsigned NOT NULL default '0',
  `role_updateDate` datetime default NULL,
  `role_updateBy` smallint(5) unsigned default '0',
  `user_id` smallint(5) unsigned default '0',
  `menu_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`role_id`),
  KEY `role_name` (`role_name`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into tbl_rolemaster(role_name, role_viwe, role_insert, role_update, role_del, role_creationDate, role_createBy, role_updateDate, role_updateBy, user_id, menu_id) VALUES
('ADMIN', 1, 1, 1, 1, '2015-11-20 16:22:31', 1, '2015-11-20 19:38:07', 1, 0, 1), 
('ADMIN', 1, 1, 1, 1, '2015-11-19 22:16:06', 1, '2015-11-20 19:38:07', 1, 0, 2), 
('ADMIN', 1, 1, 1, 1, '2015-11-17 22:30:05', 1, '2015-11-20 19:38:07', 1, 0, 3), 
('ADMIN', 1, 1, 1, 1, '2015-11-16 16:05:13', 1, '2015-11-20 19:38:07', 1, 0, 4), 
('ADMIN', 1, 1, 1, 1, '2015-11-16 16:04:33', 1, '2015-11-20 19:38:07', 1, 0, 5),
('ADMIN', 1, 1, 1, 1, '2015-11-20 21:52:05', 1, '2015-11-20 19:38:07', 0, 0, 6),
('SUPPORT', 1, 1, 1, 1, '2015-11-17 22:30:05', 1, '2015-11-20 19:38:07', 1, 0, 3), 
('SUPPORT', 1, 1, 1, 1, '2015-11-16 16:05:13', 1, '2015-11-20 19:38:07', 1, 0, 4), 
('SUPPORT', 1, 1, 1, 1, '2015-11-16 16:04:33', 1, '2015-11-20 19:38:07', 1, 0, 5),
('SUPPORT', 1, 1, 1, 1, '2015-11-20 21:52:05', 1, '2015-11-20 19:38:07', 0, 0, 6);

CREATE TABLE `tbl_menu` (
  `menu_id` smallint(5) unsigned NOT NULL auto_increment,
  `menu_group` smallint(5) unsigned NOT NULL default '0',
  `menu_header` tinyint(1) unsigned NOT NULL default '0',
  `menu_menuId` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  `menu_menuName` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `menu_icon` varchar(80) collate utf8_unicode_ci NOT NULL default 'minus',
  `menu_css` varchar(80) collate utf8_unicode_ci NOT NULL default 'dashboard',
  `menu_details` varchar(80) collate utf8_unicode_ci NOT NULL default '',
  `menu_menuUse` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `menu_for` varchar(50) collate utf8_unicode_ci NOT NULL default 'ALL',
  `menu_url` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`menu_id`),
  KEY `menu_for` (`menu_for`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into tbl_menu(menu_id, menu_group, menu_header, menu_menuId, menu_menuName, menu_icon, menu_css, menu_details, menu_menuUse, menu_for, menu_url) VALUES
(1, 69, 0, '69.1', 'Add Menu', 'minus', 'dashboard', '', 'addMenu', 'ADMIN', 'admin/addMenu.js'), 
(2, 69, 1, '69', 'Admin', 'minus', 'dashboard', '', 'admin', 'ADMIN', ''), 
(3, 68, 1, '68', 'Support Team', 'minus', 'dashboard', '', 'spt', 'SUPPORT', ''), 
(4, 68, 0, '68.1', 'Role Master', 'minus', 'dashboard', '', 'role', 'SUPPORT', 'userM/role.js'), 
(5, 68, 0, '68.2', 'Change Role', 'minus', 'dashboard', '', 'userRole', 'SUPPORT', 'userM/userRole.js'), 
(6, 68, 0, '68.3', 'Add User', 'minus', 'dashboard', '', 'add_user', 'SUPPORT', 'userM/addUser.js');

CREATE TABLE `tbl_autonumber` (
  `ID` smallint(5) unsigned NOT NULL auto_increment,
  `RunningNo` mediumint(8) unsigned default NULL,
  `GenerateDate` tinyint(1) unsigned NOT NULL default '0',
  `formatDate` varchar(20) collate utf8_unicode_ci default NULL,
  `numberReset` int(11) default NULL,
  `prefix` varchar(10) collate utf8_unicode_ci default NULL,
  `ObjectType` varchar(30) collate utf8_unicode_ci default NULL,
  `digit` tinyint(2) unsigned default NULL,
  `mReset` tinyint(1) unsigned NOT NULL default '0',
  `dayReset` tinyint(1) unsigned NOT NULL default '0',
  `creationDate` datetime default NULL,
  PRIMARY KEY  (`ID`),
  KEY `ObjectType` (`ObjectType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into tbl_autonumber(ID, RunningNo, GenerateDate, formatDate, numberReset, prefix, ObjectType, digit, mReset, dayReset, creationDate) VALUES
(1, 26, 1, '%y%m%d', 0, 'GRN', 'grn', 4, 1, 0, '2015-11-10 00:00:00'), 
(2, 1, 1, '%y%m%d', 0, 'GTN', 'gtn', 4, 0, 1, '2015-11-24 00:00:00'), 
(3, 11, 1, '%y%m%d', 0, 'PRO', 'pro', 4, 1, 0, '2015-11-10 00:00:00'), 
(4, 73, 1, '%y%m%d', 0, '', 'gen', 5, 1, 0, '2015-11-10 00:00:00'), 
(5, 27, 1, '%y%m%d', 0, '', 'snum', 5, 0, 1, '2015-11-10 00:00:00'), 
(6, 2, 1, '%y%m%d', 0, '', 'genfg', 5, 0, 1, '2015-11-24 00:00:00');

CREATE TABLE `tbl_balance_part` (
  `partNo` varchar(30) collate utf8_unicode_ci NOT NULL default '',
  `project` varchar(30) collate utf8_unicode_ci NOT NULL,
  `qty` mediumint(8) default '0',
  PRIMARY KEY  (`partNo`,`project`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tbl_printername` (
  `name` varchar(100) character set utf8 NOT NULL,
  PRIMARY KEY  (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into tbl_printername(name) values('NO_PRINT');
CREATE FUNCTION `func_GenRuningNumber`(strName VARCHAR(30),reset int(1) UNSIGNED) RETURNS varchar(30) CHARSET utf8
BEGIN
  declare _RunningNo,_numberReset,_digit int(10);
  declare _GenerateDate int(1);
  declare _formatDate,_prefix varchar(10);
  declare _creationDate datetime;
  declare returnVar varchar(30);
  declare _dayReset varchar(30);
  declare _mReset varchar(30);
  select RunningNo,numberReset,digit,GenerateDate,formatDate,prefix,creationDate,dayReset,mReset from tbl_autonumber where objectType=strName limit 1 
  into _RunningNo,_numberReset,_digit,_GenerateDate,_formatDate,_prefix,_creationDate,_dayReset,_mReset;
  if _dayReset = 1 and _mReset = 0 then
    update tbl_autonumber set RunningNo = 0 , creationDate=date_format(now(),'%Y-%m-%d 00:00:00') where ObjectType=strName and DATEDIFF(now(),creationDate) > 0;
  elseif _dayReset = 0 and _mReset = 1 then  
    update tbl_autonumber set RunningNo = 0 , creationDate=date_format(now(),'%Y-%m-%d 00:00:00') where ObjectType=strName and TIMESTAMPDIFF(MONTH,date_format(creationDate,'%Y-%m-01'),date_format(now(),'%Y-%m-01')) > 0;
  end if;  
  if _numberReset >0 and _numberReset = _RunningNo then
    update tbl_autonumber set RunningNo = 0 where ObjectType=strName;
  end if;  
  if  reset = 1 then
    update tbl_autonumber set RunningNo = 0 where ObjectType=strName;
  end if;
  update tbl_autonumber set RunningNo = RunningNo+1 where ObjectType=strName;
  select RunningNo,numberReset,digit,GenerateDate,formatDate,prefix,creationDate from tbl_autonumber where objectType=strName limit 1 
  into _RunningNo,_numberReset,_digit,_GenerateDate,_formatDate,_prefix,_creationDate;
  set returnVar = concat(_RunningNo);
  if _digit is not null then
    set returnVar = LPAD(returnVar,_digit,0);
  end if;
  if _GenerateDate = 1 then
    set returnVar = concat(date_format(now(),_formatDate),returnVar);
  end if;
  if _prefix is not null then
    set returnVar = concat(_prefix,returnVar);
  end if;
	RETURN returnVar;
END;