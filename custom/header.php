<?php
$path = $_SERVER['PHP_SELF'];
$getRootUrl = "http://".$_SERVER['HTTP_HOST'].substr($path, 0,strpos($path, '/', 1));
$imageCurrentUser = $_SESSION['xxxImage'];
$nameCurrentUser = $_SESSION['xxxFName'];
$header_menuObj = array();
function requireToVar($file){
    ob_start();
    require($file);
    return ob_get_clean();
}

$result = $mysqli->query("SELECT concat('{',group_concat(concat('\"',t2.menu_menuUse,'\":[',t1.data,']',',\"',t2.menu_menuUse,'_1_\":\"',t2.main,'\"') order by t1.menu_group separator ','),'}') data from
(select group_concat(concat('{\"value\":\"',menu_menuId,' ',menu_menuName,'\",\"id\":\"',menu_menuUse,'\",\"icon\":\"',menu_icon,'\",\"css\":\"',menu_css,'\",\"details\":\"',menu_details,'\"}')
order by substring_index(menu_menuId,'.',-1)*1 separator ',')data,menu_group 
from tbl_menu where menu_header=0 group by menu_group order by menu_group,substring_index(menu_menuId,'.',-1)*1) t1
inner join
(select menu_group,menu_menuUse,concat(menu_menuId,'. ',menu_menuName) main
from tbl_menu where menu_header=1 order by substring_index(menu_menuId,'.',-1)*1) t2 on t1.menu_group=t2.menu_group");
$data = json_decode($result->fetch_object()->data);


$header_menuByRole = array();
foreach ($data as $key => $value)
{
    if(is_bool(strpos($key,'_1_')) === true)
    {
        if($_SESSION['xxxRole']->{$key}[0])
        {
            $chRole = array();
            for($i=0,$len=count($data->{$key});$i<$len;$i++)
            {
                $id = $data->{$key}[$i]->id;
                $value = $data->{$key}[$i]->value;
                $icon = $data->{$key}[$i]->icon;
                $details = $data->{$key}[$i]->details;
                if($_SESSION['xxxRole']->{$id}[0])
                {
                    $chRole[] =
                    '{
                        id: "'.$id.'",
                        value: "'.$value.'",
                        icon: "'.$icon.'",
                        details: "'.$details.'"
                    }';
                }
            }
            $header_menuByRole[] = preg_replace('/\s{2,}/', '','
            {
                id:"'.$key.'",
                value: "'.$data->{$key.'_1_'}.'",
                open: 1,
                data: 
                [
                    '.join(',',$chRole).'
                ]
            }');
        }
    }
}

$header_menuByRole = join(',',$header_menuByRole);

$header_menu = preg_replace('/\s{2,}/', '','
var header_menu = {
    width: 230,
    rows: 
    [
        {
            view: "tree",
            id: "app:menu",
            type: "menuTree",
            css: "menu",
            activeTitle: !0,
            select: !0,
            scroll:false,
            tooltip: 
            {
                template: function(e) 
                {
                    return e.$count ? "" : e.details
                }
            },
            on: 
            {
                onBeforeSelect: function(e) 
                {
                    return this.getItem(e).$count ? !1 : void 0
                },
                onAfterSelect: function(e)
                {
                    var t = this.getItem(e);
                    $$("toolbar_title").parse({text: t.value});
                    document.title = t.value;
                    var view = $$("renderTitle");
                    for(var i=-1,len=view.getChildViews().length;++i<len;)
                    {
                        if(view.getChildViews()[i].isVisible() == true)
                        {
                            view.getChildViews()[i].hide();
                            if(view.getChildViews()[i].getChildViews()[0].callEvent instanceof Function) view.getChildViews()[i].getChildViews()[0].callEvent("onHide", []);
                        }
                    }
                    if(header_menuObj[t.id])
                    {
                        var urlArray = window.location.href.split("/");
                        var urlCut = urlArray[0]+"//"+urlArray[2]+"/"+urlArray[3]+"/"+t.id;
                        history.pushState(t.id, null, window.location.href);
                        history.replaceState(t.id, null,urlCut);
                        if($$(header_menuObj[t.id]) )
                        {
                            $$(header_menuObj[t.id]).show();
                            if($$(header_menuObj[t.id]).getChildViews()[0].callEvent instanceof Function) $$(header_menuObj[t.id]).getChildViews()[0].callEvent("onShow", []);
                        }
                        else
                        {
                            view.addView(window[header_menuObj[t.id]]());
                            $$(header_menuObj[t.id]).getChildViews()[0].callEvent("onAddView", []);
                        }
                    }
                },
                onAfterClose: function(e)
                {
                    var p = localStorage.getItem("treeMenu") || "[]";
                    p = eval("("+p+")");
                    if(p.indexOf(e) == -1)
                    {
                        p[p.length] = e;
                        localStorage.setItem("treeMenu",JSON.stringify(p));
                    }
                },
                onAfterOpen: function(e)
                {
                    var p = localStorage.getItem("treeMenu") || "[]";
                    p = eval("("+p+")");
                    if(p.indexOf(e) !== -1)
                    {
                        p.splice(p.indexOf(e),1);
                        localStorage.setItem("treeMenu",JSON.stringify(p));
                    }
                }
            },
            data:['.$header_menuByRole.']
        }
    ]
};');


$header_toolbar = preg_replace('/\s{2,}/','','
var header_toolbar = {
    view: "toolbar",
    elements: [{
        view: "label",
        label: "",
        width: 200
    }, {
        height: 46,
        id: "person_template",
        css: "header_person",
        borderless: !0,
        width: 280,
        data: {
            id: "'.$imageCurrentUser.'",
            name: "'.$nameCurrentUser.'"
        },
        template: function(e) {
            var t = "<div style=\'height:100%;width:100%;\' onclick=\'webix.$$(\"profilePopup\").show(this)\'>";
            return t += "<img class=\'photo\' src=\'images/user/" + e.id + "\' /><span class=\'name\'>" + e.name + "</span>", t += "<span class=\'webix_icon fa-angle-down\'></span></div>"
        }
    },
    {
        rows:
        [
            {
                id:"toolbar_title",
                css: "toolbar_title",
                borderless: !0,
                template: "<font size=\'5\'>#text#</font>",
                data: {
                    text: ""
                }
            }
        ],
        align:"center"
        
    },
    {
        view: "label",
        label: "",
        width: 280,
        align:"right"
    }

    ]
};');

echo $header_menu;
echo $header_toolbar;

$loadPage = array
(
    'homepage.js','profile/changePass.js','profile/profileUser.js'
);
$header_menuObj = array
(
    'history:"header_history"',
    'profileUser:"header_profileUser"',
    'homePage:"header_homePage"',
    'changepass:"header_changepass"'
);
if($re = $mysqli->query("SELECT menu_menuUse,menu_url from tbl_menu where menu_url <> ''"))
{
    for($i=0,$len=$re->num_rows;$i<$len;$i++)
    {
        $row = $re->fetch_array(MYSQLI_NUM);
        $loadPage[] = $row[1];
        $header_menuObj[] = '"'.$row[0].'":"header_'.$row[0].'"';
    }
}

echo preg_replace('/\s{2,}/', '','
var header_menuObj = 
{
    '.join(',',$header_menuObj).'
};');

for($i=0,$len=count($loadPage);$i<$len;$i++)
{
    echo preg_replace('/\s{2,}/', '',requireToVar($loadPage[$i]));
}

$mysqli->close();
?>