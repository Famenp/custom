var title = 
{
    rows: 
    [
        {
            height: 1,
            id: "title",
            css: "title",
            template: "<div class='header'></div><div class='details'></div>",
            data: {
                text: "",
                title: "Home",
                details:"หน้าแรก"
            }
        }, 
        {
            paddingX: 1,
            paddingY: 1,
            rows:
            [
            ]
        }
    ]
};
var submenu = 
{
    view: "submenu",
    id: "profilePopup",
    width: 200,
    padding: 0,
    data: [{
        id: "profileUser",
        icon: "user",
        value: "My Profile",
        details: "ข้อมูลส่วนตัว"
    }, {
        id: "changepass",
        icon: "cog",
        value: "Change Password",
        details: "เปลียนรหัสผ่าน"
    },{
        id: "logout",
        icon: "sign-out",
        value: "Logout",
        details: "ออกจากระบบ"
    }],
    type: {
        template: function(e) {
            return e.type ? "<div class='separator'></div>" : "<span class='webix_icon alerts fa-" + e.icon + "'></span><span>" + e.value + "</span>"
        }
    },
    click:function(id,e)
    {
        if('logout' == id) window.open("logOut.php","_self");
        this.hide();
        var view = $$("renderTitle"),t=this.getItem(id);
        if(document.title == t.value+" ("+t.details+")")
        {
            return;
        } 
        
        $$("toolbar_title").parse({text: t.value+" ("+t.details+")"});
        document.title = t.value+" ("+t.details+")";
        for(var i=-1,len=view.getChildViews().length;++i<len;)
        {
            if(view.getChildViews()[i].isVisible() == true)
            {
                view.getChildViews()[i].hide();
                if(view.getChildViews()[i].getChildViews()[0].callEvent instanceof Function) view.getChildViews()[i].getChildViews()[0].callEvent("onHide", []);
            }
        }

        if(header_menuObj[id])
        {
            $$("app:menu").unselectAll();
            var urlArray = window.location.href.split("/");
            var urlCut = urlArray[0]+"//"+urlArray[2]+"/"+urlArray[3]+"/"+id;
            history.pushState(id, null, window.location.href);
            history.replaceState(id, null,urlCut);
            if($$(header_menuObj[id]) )
            {
                $$(header_menuObj[t.id]).show();
                if($$(header_menuObj[id]).getChildViews()[0].callEvent instanceof Function) $$(header_menuObj[id]).getChildViews()[0].callEvent("onShow", []);
            }
            else
            {
                view.addView(window[header_menuObj[id]]());
                if($$(header_menuObj[id]))
                $$(header_menuObj[id]).getChildViews()[0].callEvent("onAddView", []);
            }
        }
    }
};
webix.ui(
    submenu
);
webix.ui(
    {   
        rows:
        [
            header_toolbar,
            {
                cols:
                [
                 
                    header_menu,{ view:"resizer" },
                    {
                        id:"renderTitle",height:window.innerHeight-53,
                        rows:
                        [

                        ]
                    }
                ]
            }
        ]
    }
);

function BufferLoader(context, urlList, callback) {
  this.context = context;
  this.urlList = urlList;
  this.onload = callback;
  this.bufferList = new Array();
  this.loadCount = 0;
}

BufferLoader.prototype.loadBuffer = function(url, index) {
  var request = new XMLHttpRequest();
  request.open("GET", url, true);
  request.responseType = "arraybuffer";

  var loader = this;

  request.onload = function() {
    loader.context.decodeAudioData(
      request.response,
      function(buffer) {
        if (!buffer) {
          alert('error decoding file data: ' + url);
          return;
        }
        loader.bufferList[index] = buffer;
        if (++loader.loadCount == loader.urlList.length)
          loader.onload(loader.bufferList);
      },
      function(error) {
        console.error('decodeAudioData error', error);
      }
    );
  };

  request.onerror = function() {
    alert('BufferLoader: XHR error');
  };

  request.send();
};

BufferLoader.prototype.load = function() {
  for (var i = 0; i < this.urlList.length; ++i)
  this.loadBuffer(this.urlList[i], i);
};


function _enable_(ar)
{
    for(var i=-1,len=ar.length;++i<len;)$$(ar[i]).enable();
}

function _disable_(ar)
{
    for(var i=-1,len=ar.length;++i<len;)$$(ar[i]).disable();
}

function comboSet(element,txt)
{
    var list = $$($$(element).config.suggest).getBody();
    list.clearAll();list.parse([txt]);list.select(list.getFirstId());list.hide();
    $$(element).setValue(txt);
}

